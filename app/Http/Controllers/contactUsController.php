<?php

namespace App\Http\Controllers;

use App\contactUs;
use Illuminate\Http\Request;

class contactUsController extends Controller
{
    public function index()
    {
        return view('pages.contact-us');
    }

    public function send(Request $request)
    {
        $data = new contactUs();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['subject'] = $request->subject;
        $data['message'] = $request->message;
        $data->save();
        return redirect()->back()->with('message', '');
    }
}
